variable "gcp_zone" {
  description = "This is your GCP zone"
  type        = string
}

variable "gcp_image" {
  description = "gcp image"
  type        = string
}

variable "gcp_machine_type" {
  description = "gcp machine type"
  type        = string
}


variable "group" {
  description = "name group of instances"
  type        = string
}

variable "tags" {

}

variable "ssh_key" {
  description = "path to your ssh private Key"
  type        = string
}

variable "ssh_pub" {
  description = "your ssh public Key"
  type        = string
}

variable "user" {
  description = "user for connection to instances with ssh public key"
  type        = string
}

variable "count_instances" {
  description = "count instances"
  type        = number
}

variable "network_name" {
  description = "network name"
}

variable "subnetwork_name" {
  description = "subnetwork name"
}
