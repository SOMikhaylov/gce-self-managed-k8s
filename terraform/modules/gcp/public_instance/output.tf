output "name_ip" {
  value = {
    for instance in google_compute_instance.instance :
    instance.network_interface[0].access_config[0].nat_ip => instance.name
  }
}

output "external_ip" {
  value = google_compute_instance.instance[*].network_interface[0].access_config[0].nat_ip
}