resource "google_compute_instance" "instance" {
  count        = var.count_instances
  name         = "${var.group}-${count.index}"
  machine_type = var.gcp_machine_type
  zone         = var.gcp_zone
  tags         = var.tags

  boot_disk {
    initialize_params {
      image = var.gcp_image
    }
  }

  network_interface {
    network    = var.network_name
    subnetwork = var.subnetwork_name
    access_config {

    }
  }

  metadata = {
    ssh-keys = "${var.user}:${file(var.ssh_pub)}"
  }

  connection {
    type         = "ssh"
    user         = var.user
    agent        = false
    private_key  = file(var.ssh_key)
    host         = self.network_interface[0].access_config[0].nat_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo '${var.group}-${count.index} up'",
    ]
  }
}