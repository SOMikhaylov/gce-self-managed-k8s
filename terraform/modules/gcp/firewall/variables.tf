variable "network_name" {
  description = "network name"
}

variable "private_ip_range" {
  default = "10.0.0.0/24"
}

# -- source ranges
variable "ssh_source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "icmp_source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "http_source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "https_source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

# -- tags
variable "ssh_target_tags" {
  description = "GCP tags"
  default     = ["ssh-server"]
}

variable "k8s_target_tags" {
  description = "GCP tags"
  default     = ["k8s-server"]
}