resource "google_compute_firewall" "firewall_ssh" {
  name    = "default-allow-ssh"
  network = var.network_name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = var.ssh_source_ranges
  target_tags   = var.ssh_target_tags
}

resource "google_compute_firewall" "firewall_k8s_nodeport" {
  name    = "default-allow-k8s-nodeport"
  network = var.network_name
  allow {
    protocol = "tcp"
    ports    = ["30000-40000"]
  }
  source_ranges = var.ssh_source_ranges
  target_tags   = var.ssh_target_tags
}

resource "google_compute_firewall" "firewall_icmp" {
  name    = "default-allow-icmp"
  network = var.network_name
  allow {
    protocol = "icmp"
  }
  source_ranges = var.icmp_source_ranges
}

resource "google_compute_firewall" "allow-all-internal" {
  name    = "allow-all-internal"
  network = var.network_name

  allow {
    protocol = "all"
  }

  source_ranges = [var.private_ip_range]
}