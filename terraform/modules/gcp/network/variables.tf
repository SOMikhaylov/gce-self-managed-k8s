variable "network_name" {
  description = "network name"
  default     = "default"
}

variable "subnetwork_name" {
  description = "subnetwork name"
  default     = "default"
}

variable "gcp_region" {
  description = "GCP region"
  type        = string
}

variable "private_ip_range" {
  default = "10.0.0.0/24"
}
