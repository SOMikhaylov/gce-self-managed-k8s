resource "local_file" "AnsibleInventory" {
  content = templatefile(var.ansible_inventory_template, {
    ansible_hosts = var.ansible_hosts
    }
  )
  filename = var.ansible_inventory
}

resource "local_file" "AnsibleConfig" {
  content = templatefile(var.ansible_cfg_template, {
    ssh_key             = var.ssh_key
    user                = var.user
    }
  )
  filename = var.ansible_cfg
}

resource "local_file" "SSHConfig" {
  content = templatefile(var.ssh_cfg_template, {
    ansible_hosts       = var.ansible_hosts
    ssh_key             = var.ssh_key
    user                = var.user
    }
  )
  filename = var.ssh_cfg
}

resource "null_resource" "ansible" {

  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command     = <<-EOT
      ANSIBLE_CONFIG=${var.ansible_cfg} ansible-playbook -i ${var.ansible_inventory} ${var.ansible_playbook}
    EOT
  }

  depends_on = [
    local_file.AnsibleInventory,
  ]
}