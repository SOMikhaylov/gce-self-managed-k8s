# GCE-SELF-MANAGED-KUBERNETES

[EN version](../README.md)

Данный проект позволяет создать простой самоуправляемый кластер `kubernetes` с помощью инструмента `kubeadm` в `Google Cloud Engine (GCE)` используя подход `Инфраструктура как код (IaC)` с помощью `terraform` и `ansible`.

Данное решение может использоваться для обучения, в том случае если вам недостаточно решения kubernetes кластера, управляемого [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) и необеспечивающего доступ к master узлам (В данном проекте создается кластер с полным доступом ко всем узлам кластера kubernetes).

Наиболее универсальным решением для создания самоуправляемого кластера kubernetes с помощью `ansible` является [kubespray](https://github.com/kubernetes-sigs/kubespray), однако данноe решениe требует слишком много времени на создание кластера. В данном проекте, используется простая и короткая ansible роль для быстрого создания и пересоздания kubernetes кластера, что обычно необходимо для учебного процесса.

---

## Использование

Для запуска и создания инфраструктуры, необходимо выполнить данные команды

```
cp terraform.tfvars.example terraform.tfvars

terraform init 
terraform plan
terraform apply -auto-approve 
```

Для удаления инфраструктуры
```
terraform destroy -auto-approve 
```

---
### Использование удаленного хранения terraform.state

Если вы хотите хранить файл `terraform.state`  в вашем удаленном хранилище, вам необходимо выполнить данные команды 

```
cp backend.tf.example backend.tf.
```

и сконфигурировать `backend.tf` для использования удаленного `backend`


```
terraform get
terraform init 
terraform plan -lock=false
terraform apply -auto-approve -lock=false
```
Для удаления инфраструктуры
```
terraform destroy -auto-approve -lock=false
```
## Используемые инструменты

 * kubeadm
 * terraform
 * ansible
 * ansible-playbook
 * google-cloud-sdk

---

## Установка

вы можете установить все данные инструменты с помощью данных гайдов 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* [Ansible install](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)


## Смотрите также

- [Terraform Google Cloud Platform Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs)

## Лицензия

MIT