# GCE-SELF-MANAGED-KUBERNETES

[RU version](docs/RU_README.md)

This project allows to create simple self-managed kubernetes cluster with `kubeadm` tool in `Google Cloud Engine (GCE)` using `Infrastucture as a Code (IaC)` approach with `terraform` and `ansible`.

This solution may be used for education if you lack management kubernetes cluster solution from [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) and don't provided access to master node (In this project created the cluster with full access for all nodes kubernetes cluster).

Most universal solution for creating self-managed kubernetes cluster with `ansible` is [kubespray](https://github.com/kubernetes-sigs/kubespray), however, this solution requires long time for creating cluster. In this case, short and simple ansible role is used for fast creating and recreating kubernetes cluster, that is usually need for education process.

---

## Usage

To create and run the infrastructure, you need to run this command

```
cp terraform.tfvars.example terraform.tfvars

terraform get
terraform init 
terraform plan
terraform apply -auto-approve 
```

To delete the infrastructure

```
terraform destroy -auto-approve 
```

---
### Usage remote storing terraform state

If you want to save `terraform.state` file on your remote storage, you need to run this command
```
cp backend.tf.example backend.tf.
```
and configure the `backend.tf` for using remote backend

```
terraform get
terraform init 
terraform plan -lock=false
terraform apply -auto-approve -lock=false
```

To delete the infrastructure
```
terraform destroy -auto-approve -lock=false
```

## Using tools

 * kubeadm
 * terraform
 * ansible
 * ansible-playbook
 * google-cloud-sdk

---

## Installation

you may install all of them tools with these guides 

* [Google Cloud sdk](https://cloud.google.com/sdk/docs/install)
* [Terraform Install](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* [Ansible install](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)


## See also

- [Terraform Google Cloud Platform Provider](https://registry.terraform.io/providers/hashicorp/google/latest/docs)

## License

MIT