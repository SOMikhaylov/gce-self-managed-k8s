terraform {
  required_version = ">= 0.13"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.90.0"
    }
  }
}

provider "google" {
  credentials = var.gcp_creds
  project     = var.gcp_project
  zone        = var.gcp_zone
}