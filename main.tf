module "network" {
  source = "./terraform/modules/gcp/network"

  network_name     = "cluster-network"
  gcp_region       = var.gcp_region
  private_ip_range = "192.168.0.0/24"
}

module "firewall" {
  source = "./terraform/modules/gcp/firewall"

  network_name     = module.network.network_name
  private_ip_range = "192.168.0.0/24"
}

module "master" {
  source = "./terraform/modules/gcp/public_instance"

  group            = "master"
  gcp_zone         = "europe-west3-a"
  gcp_image        = "ubuntu-os-cloud/ubuntu-2004-lts"
  gcp_machine_type = "e2-medium"
  user             = var.user
  ssh_pub          = var.ssh_pub
  ssh_key          = var.ssh_key
  network_name     = module.network.network_name
  subnetwork_name  = module.network.subnetwork_name
  tags             = ["ssh-server", "k8s-server"]
  count_instances  = 1
}

module "worker" {
  source = "./terraform/modules/gcp/public_instance"

  group            = "worker"
  gcp_zone         = "europe-west3-a"
  gcp_image        = "ubuntu-os-cloud/ubuntu-2004-lts"
  gcp_machine_type = "e2-medium"
  user             = var.user
  ssh_pub          = var.ssh_pub
  ssh_key          = var.ssh_key
  network_name     = module.network.network_name
  subnetwork_name  = module.network.subnetwork_name
  tags             = ["ssh-server", "k8s-server"]
  count_instances  = 1
}


module "provision" {
  source = "./terraform/modules/ansible_provision"

  ansible_inventory_template = "ansible/inventory.tmpl"
  ansible_inventory          = "ansible/inventory.ini"
  ansible_requirements       = "ansible/requirements.yml"
  ansible_playbook           = "ansible/site.yml"
  ansible_cfg_template       = "ansible/ansible.cfg.tmpl"
  ansible_cfg                = "ansible/ansible.cfg"
  ssh_cfg_template           = "ansible/ssh.cfg.tmpl"
  ssh_cfg                    = "ansible/ssh.cfg"
  ansible_hosts = {
    master = module.master.name_ip
    worker      = module.worker.name_ip
  }
  ssh_key             = var.ssh_key
  user                = var.user
}