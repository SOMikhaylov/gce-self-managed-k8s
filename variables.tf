variable "gcp_creds" {
  description = "Either the path to or the contents of a service account key file in JSON format"
  type        = string
}

variable "gcp_project" {
  description = "your GCP project "
  type        = string
}

variable "gcp_zone" {
  description = "GCP zone"
  type        = string
}

variable "gcp_region" {
  description = "GCP region"
  type        = string
}

variable "ssh_key" {
  description = "path to your ssh private Key"
  type        = string
}

variable "ssh_pub" {
  description = "your ssh public Key"
  type        = string
}

variable "user" {
  description = "user for connection to instances with ssh public key"
  type        = string
}